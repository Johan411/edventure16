<!doctype html>
<html>
       
<head>
    <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="EDventure 2016, a novel venture initiated by
the Entrepreneurship Development Cell(CET-EDC) of the
College of Engineering Trivandrum, aims to
ignite the spark of entrepreneurship in
students across the state. cet cetedc edccet edventurecet">
<link rel="shortcut icon" href="img/icon.ico" />
    <title>EDVENTURE'16</title>
    



    <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/anim.css" rel="stylesheet">


<!--[if lte IE 8]>
  
    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-old-ie-min.css">
  
<![endif]-->
<!--[if gt IE 8]><!-->
  
    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-min.css">
  
<!--<![endif]-->



<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">



  
    <!--[if lte IE 8]>
        <link rel="stylesheet" href="css/layouts/marketing-old-ie.css">
    <![endif]-->
    <!--[if gt IE 8]><!-->
        <link rel="stylesheet" href="css/marketing.css">
                <link rel="stylesheet" href="style.css">
    <!--<![endif]-->
  


    

    

</head>
<body style="color:white;" onload="warning();">

<div class="header">
    <div class="pure-g-r">
        <div class="home-menu pure-menu pure-menu-horizontal pure-menu-fixed" style="background-color:white;">
            <div class="pure-u-1-3">
            <img id="animation" src="img/bgl.png" width="400" height="100"/>
            </div>
            <div class="pure-u-1-3">
            </div>
            <div class="pure-u-1-3"  style="margin-left:2.5%;margin-top:20px;">
            <h2 >Event Slots Selection</h2>
            </div>
        </div>
    </div>
</div>


<div class="row" style="background-color:#333;margin-top:120px;text-align:center;width:102%">
    <h3 class="text" style="color:white;"><b>Plan your schedule</b></h3>
<br>

<form class="form-inline" method="POST" name="regform" action="form_process.php">
 <div class="row">
            <div class="pure-u-1-4">
            </div>
            <div class="pure-u-1-4">
            <h1 class="text" style="color:white;"> DAY 1</h1>
            </div>
            <div class="pure-u-1-4">
            <h3><a class="btn btn-default" href="event.html" target="_blank">EVENTS</a> <a class="btn btn-default" href="shed.html"
            target="_blank" >SCHEDULE</a></h3>
            </div>
</div>

<fieldset>
<div>
<h3 style="color:#ADD8E6;">Slot: 9:30AM - 11AM </h3>
        <div class="pure-u-1-4">
            <input id="radio-1" class="radio-custom" name="Slot 1" type="radio" value="Panel Discussion" onclick="checkslots();">
            <label for="radio-1" class="radio-custom-label rad">Panel Discussion</label>
        </div>
        <div class="pure-u-1-4">
            <input id="radio-2" class="radio-custom" name="Slot 1" type="radio" value="Ideation Workshop 1" onclick="checkslots();">
            <label for="radio-2" class="radio-custom-label rad">Ideation Workshop</label>
        </div>
        <div class="pure-u-1-4">
            <input id="radio-3" class="radio-custom" name="Slot 1" type="radio" value="Ideation Workshop 2" onclick="checkslots();">
            <label for="radio-3" class="radio-custom-label rad">Teamwork & Leadership Workshop</label>
        </div>
<h3 style="color:#ADD8E6;">Slot: 11AM - 1PM</h3>
        <div class="pure-u-1-4">
            <input id="radio-4" class="radio-custom" name="Slot 2" type="radio" value="Talk Series 1" onclick="checkslots();">
            <label for="radio-4" class="radio-custom-label rad">Talk Series 1</label>
        </div>
        <div class="pure-u-1-4"> 
            <input id="radio-5" class="radio-custom" name="Slot 2" type="radio" value="Workshop 1" onclick="checkslots();">
            <label for="radio-5" class="radio-custom-label rad">Product Design Workshop</label>
        </div>
        <div class="pure-u-1-4">
            <input id="radio-6" class="radio-custom" name="Slot 2" type="radio" value="Workshop 2" onclick="checkslots();">
            <label for="radio-6" class="radio-custom-label rad">Company Branding Workshop</label>
        </div>
<h3 style="color:#ADD8E6;">Slot: 2PM - 4PM </h3>
        <div class="pure-u-1-4">
            <input id="radio-7" class="radio-custom" name="Slot 3" type="radio" value="Women Entrepreneurship" onclick="checkslots();">
            <label for="radio-7" class="radio-custom-label rad">Gender In Entrepreneurship</label>
        </div>
        <div class="pure-u-1-4">
            <input id="radio-8" class="radio-custom" name="Slot 3" type="radio" value="Get Set Pitch" onclick="checkslots();" onfocus="notification();">
            <label for="radio-8" class="radio-custom-label rad">Get Set Pitch</label>
        </div>
        <div class="pure-u-1-4">
            <input id="radio-9" class="radio-custom" name="Slot 3" type="radio" value="Case Study Competition" onclick="checkslots();" onfocus="notification();">
            <label for="radio-9" class="radio-custom-label rad">Case Analysis Competition</label>
        </div>
</div>
</fieldset>
<h1 class="text" style="color:white;"> DAY 2</h1>
<fieldset>
<div>
<h3 style="color:#ADD8E6;">Slot: 9:00AM - 11AM </h3>
        <div class="pure-u-1-4">
            <input id="radio-13" class="radio-custom" name="Slot 4" type="radio" value="Case Study by Business Analyst" onclick="checkslots();">
            <label for="radio-13" class="radio-custom-label rad">Q&A with C Balagopal <br>&<br> The Zomato Review</label>
        </div>
        <div class="pure-u-1-4">
            <input id="radio-14" class="radio-custom" name="Slot 4" type="radio" value="Workshop 3" onclick="checkslots();">
            <label for="radio-14" class="radio-custom-label rad">Legal Aspects of starting business 1</label>
        </div>
        <div class="pure-u-1-4">
            <input id="radio-15" class="radio-custom" name="Slot 4" type="radio" value="Workshop 4" onclick="checkslots();">
            <label for="radio-15" class="radio-custom-label rad">Legal Aspects of starting business 2</label>
        </div>
<h3 style="color:#ADD8E6;">Slot: 11:15AM - 12:45PM</h3>
        <div class="pure-u-1-3">
            <input id="radio-16" class="radio-custom" name="Slot 5" type="radio" value="Talk Series 2" onclick="checkslots();">
            <label for="radio-16" class="radio-custom-label rad">Talk Series 2</label>
        </div>
        <div class="pure-u-1-3">
            <input id="radio-17" class="radio-custom" name="Slot 5" type="radio" value="StartUp Expo" onclick="checkslots();">
            <label for="radio-17" class="radio-custom-label rad">StartUp Expo</label>
        </div>
</fieldset>
<h1 class="text" style="color:white;">Food</h1>

<fieldset>
<div class="pure-u-1-3">
            <input id="radio-25" class="radio-custom" name="food" type="radio" value="Veg">
            <label for="radio-25" class="radio-custom-label rad">Veg</label>
        </div>
        <div class="pure-u-1-3">
            <input id="radio-26" class="radio-custom" name="food" type="radio" value="Non-Veg">
            <label for="radio-26" class="radio-custom-label rad">Non-Veg</label>
        </div>
</fieldset>
<input class="btn btn-primary" type="submit" name="submit" value="SUBMIT" style="margin-top:20px;" onclick="foodnotif();">
</form>



    <div class="footer l-box is-center" style="color:white;margin-top:100px;">
        <b>Contact Us</b>
        <div class="pure-g-r" style="text-align:left;">
            <div class="l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-4" style="text-align:center"><br><br>
                <a href="https://www.facebook.com/edventurecet/?ref=hl"><img src="img/facebook.png" width="150" height="150"></a>
            </div>
             <div class="l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-4" style="text-align:center">
                <br>
                <b>Address:</b> EDC-CET </br>Room no. A-204<br/>Main building 1st floor</br>College of Engineering Trivandrum</br>
	        				<b>Phone:</b> 9496988298<br/>
	        				
    <b>Email:</b> <a href="mailto:edc.cet@gmail.com">edc.cet@gmail.com</a></br>
            </div>
            
            <div class="l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-4" style="text-align:center">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1972.755337633533!2d76.904406!3d8.546792!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x98324eb5aafb3778!2sCollege+of+Engineering+Trivandrum!5e0!3m2!1sen!2sus!4v1456564765064" width="430" height="210" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div> 
           
            
        </div>
        &copy;EDC-CET 2016
</div>

<script type="text/javascript">
    
    function notification(){
     
        //alert("Don't select this if you are a participant for the competition");
        
    }
    
    function warning(){
        
        alert("Limited seats available for each session. Details once submitted cannot be modified.");
    }
    
    function foodnotif(){
        
        alert("Kindly note that lunch and refreshments will be provided on each day");
        
    }
    
    function checkslots(){
        var t1=document.getElementsByName("Slot 1");
        var t2=document.getElementsByName("Slot 2");
        var t3=document.getElementsByName("Slot 3");
        var t4=document.getElementsByName("Slot 4");
        var t5=document.getElementsByName("Slot 5");
        var food=document.getElementsByName("food");
        var i;
        
        if(t1[1].checked==true || t1[2].checked==true)
        {
            if(t2[1].checked==true || t2[2].checked==true)
            {
                alert("Cannot select more than one workshop a day!");
                t2[1].checked=false;
                t2[2].checked=false;
                t1[1].checked=false;
                t1[2].checked=false;
            }
        }
        /*
        if(t1[1].checked==true || t1[2].checked==true)
        {
            if(t4[1].checked==true || t4[2].checked==true)
            {
                alert("Cannot select Ideation Workshop on Day 1 and Workshop 3 or 4 on Day 2!");
                t1[1].checked=false;
                 t1[2].checked=false;
                 t4[1].checked=false;
                 t4[2].checked=false;
            }
        }*/

    }

        
</script>

 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/smooth.js"></script>

</body>
</html>
